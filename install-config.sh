#!/bin/bash

cp -Rv config/* ~/.config/

gsettings set org.gnome.desktop.interface icon-theme Papirus
gsettings set org.gnome.desktop.interface gtk-theme 'Adwaita-dark'
gsettings set org.gnome.desktop.interface font-name 'Inter Medium 11'
gsettings set org.gnome.desktop.interface monospace-font-name 'Iosevka Term 11'

