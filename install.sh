#!/bin/bash

set -e
set -x

# TODO: Iosevka font (included in reform-system-image)
sudo apt install build-essential meson cmake fonts-inter papirus-icon-theme wofi network-manager-gnome wofi librsvg2-dev hwdata libayatana-appindicator1
sudo apt build-dep wayfire waybar

# Wayfire
git clone https://github.com/WayfireWM/wayfire
cd wayfire
patch -p1 < ../patches/issue-1750-scale.patch
patch -p1 < ../patches/wf-initial-deco-size.patch
#meson build -Duse_system_wfconfig=enabled -Denable_gles32=false
meson build -Duse_system_wfconfig=enabled -Denable_gles32=false -Doptimization=1 build
sudo ninja -C build install
cd ..

# Waybar
git clone --branch 0.9.15 https://github.com/Alexays/Waybar.git
cd Waybar
patch -p1 < ../patches/waybar-hotfix-desktop-file.patch
meson build
sudo ninja -C build install
cd ..

# Firedecor (our fork)
git clone https://github.com/mntmn/Firedecor.git
cd Firedecor
meson build
sudo ninja -C build install
cd ..

# wayfire-shadows (our fork)
#git clone https://github.com/mntmn/wayfire-shadows.git
#cd wayfire-shadows
#meson build
#sudo ninja -C build install
#cd ..

# Config and styles
./install-config.sh
